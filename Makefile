LFLAGS=-L/usr/lib -laf -L/usr/lib64 -llpsolve55
CFLAGS=--std=c++11 -Iinclude -I/usr/include
EXE=benchmark

benchmark: src/main.cpp src/af_solver.cpp src/lp_solver.cpp src/utilities.cpp
	$(CXX) $(CFLAGS) $(LFLAGS) src/main.cpp src/af_solver.cpp src/lp_solver.cpp src/utilities.cpp -o $(EXE)

all: benchmark
