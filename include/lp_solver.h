#include <lpsolve/lp_lib.h>

#include "solver.h"

#pragma once

class LpSolveSolver : public Solver
{
public:

  LpSolveSolver( int numVariables ) : Solver(numVariables)
  {
    mLp = make_lp(0, numVariables);    // Add default number of columns
    set_add_rowmode(mLp, TRUE);        // Adding rows, so rowmode performs better
  }

  ~LpSolveSolver()
  {
    delete_lp(mLp);
  }
  
  void solve();

protected:
  void addEquation_Impl( const std::vector<float> &rowAx_i, float rowBx_i );

private:
  lprec *mLp;
};
