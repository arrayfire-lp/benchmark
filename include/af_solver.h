#include <arrayfire.h>

#include "solver.h"

#pragma once

class ArrayFireSolver : public Solver
{
public:
  using Solver::Solver;
  void solve();
  
protected:
  void addEquation_Impl( const std::vector<float> &rowAx_i, float rowBx_i );

private:
  af::array mA;
  af::array mB;
  int mCurrentRow;
};
