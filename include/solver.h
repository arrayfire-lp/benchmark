// -*- mode: c++ -*-

#pragma once

#include <vector>
#include <cassert>

/**
 * An interface for solving simple Linear Programming (LP) problems of the form Ax=B.
 *
 * Each problem is initialized with a number of  
 *   >> solver = Solver( 4 );
 *   >> solver.addRow( {1, 2, 3}, 4 );
 *   >> solver.addRow();
 */
class Solver
{
public:

  Solver( int numVariables ) :
    mNumVariables( numVariables )
  {};
  
  void addEquation( const std::vector<float> &rowAx_i, float rowBx_i )
  {
    assert( rowAx_i.size() == mNumVariables );
    addEquation_Impl( rowAx_i, rowBx_i );
  }

  virtual void solve() = 0;

protected:
  virtual void addEquation_Impl( const std::vector<float> &rowAx_i, float rowBx_i ) = 0;

private:
  int mNumVariables;
      
};
