#pragma once

/**
 * Generates an LP problem of the form Ax=B with a given number of variables and equations.
 */
void generate_lp( int numVariables, int numEquations,
		  std::vector< std::vector<float> > &matA,
		  std::vector<float> &matB );
