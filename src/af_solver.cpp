#include <vector>
#include <iostream>

#include <arrayfire.h>

#include "af_solver.h"

void ArrayFireSolver::addEquation_Impl( const std::vector<float> &rowAx_i, float rowBx_i )
{
  af::array row_Ax_i( 1, rowAx_i.size(), rowAx_i.data() );
  
  af::array mat = af::join( mCurrentRow, mA, row_Ax_i );
  mA = mat;

  af::array matB_row( 1, 1, &rowBx_i );
  af::array matB = af::join( mCurrentRow, mB, matB_row );
  mB = matB;
}

void ArrayFireSolver::solve()
{
  af::array x = af::solve( mA, mB );
}
