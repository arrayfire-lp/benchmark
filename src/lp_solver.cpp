#include <vector>
#include <cassert>

#include <lpsolve/lp_lib.h>

#include "lp_solver.h"

const auto& lp_solve = solve; // Redefine because we overwrite 'solve' inside the class

void LpSolveSolver::addEquation_Impl( const std::vector<float> &rowAx_i, float rowBx_i )
{
  // Not sure why we need vec.data() - 1, but otherwise the columns are shifted
  // over by one. Couldn't find anything in the docs about why, but it is what
  // it is I guess.

  std::vector<double> vec( rowAx_i.begin(), rowAx_i.end() );
  add_constraint( mLp, vec.data() - 1, EQ, rowBx_i );
}

void LpSolveSolver::solve()
{
  int result = lp_solve(mLp); // lp_solve --> lp_lib.h::solve
  // Note: May want some sort of legitimate result
}
