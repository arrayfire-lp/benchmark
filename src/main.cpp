#include <algorithm>
#include <iostream>
#include <chrono>
#include <stdlib.h>

#include <arrayfire.h>

#include "solver.h"
#include "af_solver.h"
#include "lp_solver.h"
#include "utilities.h"

typedef std::chrono::high_resolution_clock chrono_clock;
typedef std::chrono::duration<double, std::ratio<1>> chrono_second;

void print_usage( const std::string &progname )
{
  std::cout << "Usage: " << progname << " NUM_VARIABLES NUM_EQUATIONS" << std::endl;
  std::cout << std::endl;
  std::cout << "Runs a benchmark comparison between LpSolve and ArrayFire::solve." << std::endl;
  std::cout << "" << std::endl;
  std::cout << "Arguments:" << std::endl;
  std::cout << "  NUM_VARIABLES: The number of variables to solve for in the LP" << std::endl;
  std::cout << "  NUM_EQUATIONS: The number of equations in the LP. Must be >= NUM_VARIABLES" << std::endl;
}

int main(int argc, char* argv[])
{
  if ( argc != 3 )
  {
    print_usage( argv[0] );
    exit(1);
  }

  int num_vars      = atoi( argv[1] );
  int num_equations = atoi( argv[2] );

  if ( num_equations < num_vars )
  {
    std::cerr << "Error! NUM_EQUATIONS must be >= NUM_VARIABLES" << std::endl;
    std::cerr << std::endl;
    exit(1);
  }
  
  std::vector< std::vector<float> > equations;
  std::vector< float > results;

  generate_lp( num_vars, num_equations, equations, results );

  /**************************************************************************
   * ArrayFire
   **************************************************************************/
  {
    chrono_clock::time_point start_time = chrono_clock::now();
    ArrayFireSolver af_solver( num_vars );
    for ( int i = 0; i < num_equations; ++i )
    {
      af_solver.addEquation( equations[i], results[i] );
    }
    chrono_clock::time_point solve_start_time = chrono_clock::now();
    af_solver.solve();
    chrono_clock::time_point end_time = chrono_clock::now();

    chrono_second duration = std::chrono::duration_cast<chrono_second>( end_time - start_time );
    chrono_second solve_duration = std::chrono::duration_cast<chrono_second>( end_time - solve_start_time );
    std::cout << "ArrayFire implementation: ";
    std::cout << std::fixed << duration.count();
    std::cout << " " << std::fixed << solve_duration.count() << std::endl;
  }

  /**************************************************************************
   * LpSolve
   **************************************************************************/
  {
    chrono_clock::time_point start_time = chrono_clock::now();
    LpSolveSolver lp_solver( num_vars );
    for ( int i = 0; i < num_equations; ++i )
    {
      lp_solver.addEquation( equations[i], results[i] );
    }
    chrono_clock::time_point solve_start_time = chrono_clock::now();
    lp_solver.solve();
    chrono_clock::time_point end_time = chrono_clock::now();

    chrono_second duration = std::chrono::duration_cast<chrono_second>( end_time - start_time );
    chrono_second solve_duration = std::chrono::duration_cast<chrono_second>( end_time - solve_start_time );
    std::cout << "LpSolve implementation: ";
    std::cout << std::fixed << duration.count();
    std::cout << " " << std::fixed << solve_duration.count() << std::endl;
  }

  return 0;
}
