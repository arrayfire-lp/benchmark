#include <functional>
#include <vector>
#include <algorithm>
#include <numeric>

#include "utilities.h"

void generate_lp( int numVariables, int numEquations,
		  std::vector< std::vector<float> > &matA,
		  std::vector<float> &matB )
{
  // Generate 'x' component of LP problem
  std::vector< float > x( numEquations );
  std::generate( x.begin(), x.end(), std::rand );

  // Generate 'A' component of LP problem, then calculate B from current equation
  matA.resize( numEquations, std::vector<float>(numVariables) );
  matB.resize( numEquations );
  
  for ( int i = 0; i < numEquations; ++i )
  {
    std::vector<float> &coefficients = matA[i];
    std::generate( coefficients.begin(), coefficients.end(), std::rand );

    std::vector<float> equation( coefficients.size() );
    std::transform( coefficients.begin(), coefficients.end(),
		    x.begin(), equation.begin(),
		    std::multiplies<float>() );

    // Calculate values of B from random Ax equations
    matB[i] = std::accumulate( equation.begin(), equation.end(), 0, std::plus<float>() );
  }
}
